
#pip install PyPDF2
import os   #to create a directory
import PyPDF2  #a python library built as a PDF toolkit. It is capable of extracting document information,splitting documents page and more

pdf_path = 'specify the path to the PDF file'

# Open the PDF file in binary mode
with open(pdf_path, 'rb') as pdf_file:
    # Create a PDF reader object
    pdf_reader = PyPDF2.PdfReader(pdf_file)

    # Initialize an empty string to store extracted text
    extracted_text = ""

    # Loop through all pages and extract text
    for page_num in range(len(pdf_reader.pages)):
        # Get a page object
        page_obj = pdf_reader.pages[page_num]

        # Extract text from the page
        extracted_text += page_obj.extract_text()


output_folder = 'specify the path for the output folder and file'
output_file_path = os.path.join(output_folder, 'output_text.txt')

# Create the output folder if it doesn't exist
os.makedirs(output_folder, exist_ok=True)

# Open the output text file in write mode
with open(output_file_path, 'w', encoding='utf-8') as output_file:
    # Write the extracted text to the file
    output_file.write(extracted_text)

print(f"Text extracted and saved to: {output_file_path}")

