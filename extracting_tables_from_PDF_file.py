#pip install PyPDF2
#pip install tabula-py
#pip install tabulate

import os
import PyPDF2
import tabula
from tabulate import tabulate


pdf_path = 'specify the path to the PDF file'

# Use tabula to extract tables
tables = tabula.read_pdf(pdf_path, pages='all', multiple_tables=True)

output_folder = 'specify the path for the output folder and files'
output_table_text_file_path = os.path.join(output_folder, 'output_table_text.txt')

# Create the output folder if it doesn't exist
os.makedirs(output_folder, exist_ok=True)


# Open the output table text file in write mode
with open(output_table_text_file_path, 'w', encoding='utf-8') as output_table_text_file:
    # Convert tables to a text format and write to the file
    for i, table in enumerate(tables):
        # Check the number of columns in the table
        num_columns = len(table.columns)

        # Decide whether to treat it as text or a table
        if num_columns > 1:
            table_text = tabulate(table, headers='keys', tablefmt='grid')
            output_table_text_file.write(f"Table {i + 1}:\n")
            output_table_text_file.write(table_text)
            output_table_text_file.write('\n\n')
        else:
            # Treat as text
            output_table_text_file.write(f"Text {i + 1}:\n")
            output_table_text_file.write(table.iloc[:, 0].to_string(index=False))
            output_table_text_file.write('\n\n')

print(f"Text and tables extracted and saved to: {output_folder}")
